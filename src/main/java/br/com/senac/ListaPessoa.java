
package br.com.senac;

import br.com.senac.ex1.model.Pessoa;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class ListaPessoa {
    
    public static void main(String[] args) {
        List<Pessoa> lista = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        
        Pessoa p = new Pessoa();
        System.out.println("Nome: ");
        p.setNome(scanner.nextLine());
        System.out.println("Idade: ");
        p.setIdade(scanner.nextInt());
        
        Pessoa p1 = new Pessoa();
        System.out.println("Nome: ");
        p1.setNome(scanner.nextLine());
        System.out.println("Idade: ");
        p1.setIdade(scanner.nextInt());
        
        Pessoa p2 = new Pessoa();
        System.out.println("Nome: ");
        p2.setNome(scanner.nextLine());
        System.out.println("Idade: ");
        p2.setIdade(scanner.nextInt());
        
        lista.add(p);
        lista.add(p1);
        lista.add(p2);
        
        Pessoa pMaisVelha = getPessoaMaisVelha(lista);
        Pessoa pMaisNova = getPessoaMaisNova(lista);
        System.out.println("Pessoa mais velha é: " + pMaisVelha.getNome() + " com " + pMaisVelha.getIdade() + "Anos.");
        System.out.println("Pessoa mais nova é: " + pMaisNova.getNome() + " com " + pMaisNova.getIdade() + "Anos.");
    }
    
    public static Pessoa getPessoaMaisNova(List<Pessoa> lista) {
        Pessoa pessoaMaisNovo = lista.get(0);
        
        for(Pessoa p : lista){
            if (p.getIdade() < pessoaMaisNovo.getIdade()){
                pessoaMaisNovo = p;
            }
        }
        
        return pessoaMaisNovo;
        
    }
    
    
    public static Pessoa getPessoaMaisVelha(List<Pessoa> lista){
        
        Pessoa pessoaMaisVelha = lista.get(0);
        
        for (Pessoa p : lista){
            if (p.getIdade() > pessoaMaisVelha.getIdade()){
                pessoaMaisVelha = p;
            } 
        }
        
        
        
        
        return pessoaMaisVelha;
    }
   
}
